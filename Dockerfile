FROM node:9.11.2
RUN apt-get update && apt install git curl tar -y
RUN mkdir clusterio

RUN git clone -b master https://bitbucket.org/jordanond/clusterio.git && cd clusterio && npm install
RUN cd clusterio && curl -o factorio.tar.gz -L https://www.factorio.com/get-download/latest/headless/linux64 && tar -xf factorio.tar.gz

ENV FACTORIO_PORT 34167
ENV INSTANCE_TYPE master #must be either client or master
ENV INSTANCE_NAME instance_name

WORKDIR clusterio
RUN mkdir instances sharedMods
RUN cp config.json.dist config.json

RUN node client.js download

LABEL maintainer "jondrusek@gmail.com"

VOLUME /clusterio/instances
VOLUME /clusterio/sharedMods
VOLUME /clusterio/sharedPlugins

CMD RCONPORT="$RCONPORT" FACTORIOPORT="$FACTORIOPORT" MODE="$MODE" node $MODE\$INSTANCE_TYPE.js start $INSTANCE_NAME
